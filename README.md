Demonstrate the Activity Streams API by building a sample plugin that uses a JIRA issue event listener to add an activity whenever an issue is deleted.

You can find the entire tutorial at: 

https://developer.atlassian.com/display/DOCS/Adding+Activities+to+a+Third+Party+feed+with+the+Java+API