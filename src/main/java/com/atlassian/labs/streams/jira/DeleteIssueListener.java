package com.atlassian.labs.streams.jira;

import java.net.URI;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.api.Application.application;

public class DeleteIssueListener implements InitializingBean, DisposableBean
{
    private final EventPublisher eventPublisher;
    private final ActivityService activityService;
    private final ApplicationProperties applicationProperties;
    private static final Logger log = LoggerFactory.getLogger(DeleteIssueListener.class);

    public DeleteIssueListener(EventPublisher eventPublisher,
                               ActivityService activityService,
                               ApplicationProperties applicationProperties)
    {
        this.eventPublisher = eventPublisher;
        this.activityService = activityService;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void destroy() throws Exception
    {
        eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        eventPublisher.register(this);
    }

    @EventListener
    public void onIssueDelete(IssueEvent issueEvent)
    {
        if (issueEvent.getEventTypeId().equals(EventType.ISSUE_DELETED_ID))
        {
            User user = issueEvent.getUser();
            Either<ValidationErrors, Activity> result =
                new Activity.Builder(application("JIRA Deleted Issues", URI.create(applicationProperties.getBaseUrl())),
                                     new DateTime(),
                                     new UserProfile.Builder(user.getName()).fullName(user.getDisplayName()).build())
                    .content(some(html(issueEvent.getIssue().getDescription())))
                    .title(some(html(user.getDisplayName() + " deleted " + issueEvent.getIssue().getKey() + " - " + issueEvent.getIssue().getSummary())))
                    .url(some(URI.create(applicationProperties.getBaseUrl())))
                    .build();
            for (Activity activity : result.right())
            {
                activityService.postActivity(activity);
            }
            for (ValidationErrors errors : result.left())
            {
                log.error("Errors encountered attempting to post activity: " + errors.toString());
            }
        }
    }
}
